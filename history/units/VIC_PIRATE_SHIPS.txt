units = {
	navy = {				
		name = "Pirate Ships"			
		base = 4447
		location = 4447
		ship = { name = "Queen Anne" definition = destroyer equipment = { destroyer_equipment_2 = { amount = 1 owner = VIC } } }
		ship = { name = "Black Flag" definition = destroyer equipment = { destroyer_equipment_2 = { amount = 1 owner = VIC } } }
		ship = { name = "Graven's Second Favorite Boat" definition = destroyer equipment = { destroyer_equipment_2 = { amount = 1 owner = VIC } } }
		ship = { name = "He's A Pirate" definition = destroyer equipment = { destroyer_equipment_2 = { amount = 1 owner = VIC } } }
		ship = { name = "Blue Flag" definition = destroyer equipment = { destroyer_equipment_2 = { amount = 1 owner = VIC } } }
		ship = { name = "Keelhaulin'" definition = destroyer equipment = { destroyer_equipment_2 = { amount = 1 owner = VIC } } }
		ship = { name = "Zergie" definition = destroyer equipment = { destroyer_equipment_2 = { amount = 1 owner = VIC } } }
		ship = { name = "Raider 1" definition = destroyer equipment = { light_cruiser_equipment_1 = { amount = 1 owner = VIC } } }
		ship = { name = "Raider 2" definition = destroyer equipment = { light_cruiser_equipment_1 = { amount = 1 owner = VIC } } }
		ship = { name = "Raider 3" definition = destroyer equipment = { light_cruiser_equipment_1 = { amount = 1 owner = VIC } } }
		ship = { name = "Raider 5" definition = destroyer equipment = { light_cruiser_equipment_1 = { amount = 1 owner = VIC } } }
		ship = { name = "Brora" definition = heavy_cruiser equipment = { heavy_cruiser_equipment_1 = { amount = 1 owner = VIC } } }
		ship = { name = "Inviolable Rights" definition = heavy_cruiser equipment = { heavy_cruiser_equipment_1 = { amount = 1 owner = VIC } } }
		ship = { name = "Graven's Favorite Boat" definition = heavy_cruiser equipment = { heavy_cruiser_equipment_1 = { amount = 1 owner = VIC } } }
	}
	
}